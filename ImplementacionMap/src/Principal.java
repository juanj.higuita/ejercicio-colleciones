import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public class Principal {
    public static void main(String[] args) {

        // Hagamos de cuenta que el map va a representar los jugadores de un equipo que estan en la cancha
        // Cada uno tiene un dorsal diferente a los demas, esta sera su key y el valor va a ser su nombre, un String
        // El Map es como si fuera el campo de futbol y los futbolistas estan en el campo.

        System.out.println("Ejemplo con HashMap");System.out.println("");
        Map<Integer, String> treeMap = new TreeMap<Integer, String>();
        treeMap.put(1, "Casillas");	treeMap.put(15, "Ramos");
        treeMap.put(3, "Pique");	treeMap.put(5, "Puyol");
        treeMap.put(11, "Capdevila");	treeMap.put(14, "Xabi Alonso");
        treeMap.put(16, "Busquets");	treeMap.put(8, "Xavi Hernandez");
        treeMap.put(18, "Pedrito");	treeMap.put(6, "Iniesta");
        treeMap.put(7, "Villa");

        // Imprimimos el Map con un Iterador que ya hemos instanciado anteriormente
        Iterator<Integer> it = treeMap.keySet().iterator();
        while(it.hasNext()){
            Integer key = it.next();
            System.out.println("Clave: " + key + " -> Valor: " + treeMap.get(key));
        }

        System.out.println("");
        System.out.println("********* Ahora los metodos que se usan con el Map *********");
        System.out.println("Cuantos jugadores hay?:= "+treeMap.size());
        System.out.println("Esta vacio el Campo? : = "+treeMap.isEmpty());
        System.out.println("El futbolista con el dorsal 6 es:  = "+treeMap.get(6));
        System.out.println("Lesion del jugador de la dorsal 18, se va del campo lesionado: "+treeMap.remove(18));
        System.out.println("Si quiero saber el dorsal de alguien que no esta en el partido porque lo sustituyeron:  = "+treeMap.get(18));
        System.out.println("Existe un jugador con el dorsal 18?:  = "+treeMap.containsKey(18));
        System.out.println("Esta jugando un jugador con el dorsal 1?:  = "+treeMap.containsKey(1));
        System.out.println("Villa esta jugando?:  = "+treeMap.containsValue("Villa"));
        System.out.println("Ricardo esta jugando?:  = "+treeMap.containsValue("Ricardo"));
        System.out.println("Se acabo el partido, todos los jugadores se fueron: Se eliminaron los jugadores del Map");treeMap.clear();
        System.out.println("Sera que en serio no hay nadie? Veamos si hay algun jugador en el campo: = "+treeMap.size());
        System.out.println("Esta vacio el Map?  = "+treeMap.isEmpty());

    }
}
